package com.example.blogapplication.ui.home

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.blogapplication.R
import com.example.blogapplication.core.Result
import com.example.blogapplication.core.hide
import com.example.blogapplication.core.show
import com.example.blogapplication.data.model.Post
import com.example.blogapplication.data.remote.HomeScreenDataSource
import com.example.blogapplication.databinding.FragmentHomeScreenBinding
import com.example.blogapplication.domain.home.HomeScreenRepositoryImpl
import com.example.blogapplication.presentation.main.HomeScreenViewModel
import com.example.blogapplication.presentation.main.HomeScreenViewModelFactory
import com.example.blogapplication.ui.home.adapter.HomeScreenAdapter
import com.example.blogapplication.ui.home.adapter.OnPostClickListener
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class HomeScreenFragment : Fragment(R.layout.fragment_home_screen) , OnPostClickListener{

    private lateinit var binding: FragmentHomeScreenBinding
    private val viewModel by viewModels<HomeScreenViewModel> { HomeScreenViewModelFactory(HomeScreenRepositoryImpl(
        HomeScreenDataSource()
    )) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentHomeScreenBinding.bind(view)

        /*val postList = listOf(
            Post("https://media.istockphoto.com/photos/paperless-workplace-idea-esigning-electronic-signature-document-an-picture-id1349390515?k=20&m=1349390515&s=612x612&w=0&h=Q1eF3EPRjhyxT7WXFxXnq0GkhyqjusV42dyBUIgY4_A=", "Gaston", Timestamp.now(), "https://media.istockphoto.com/photos/paperless-workplace-idea-esigning-electronic-signature-document-an-picture-id1349390515?k=20&m=1349390515&s=612x612&w=0&h=Q1eF3EPRjhyxT7WXFxXnq0GkhyqjusV42dyBUIgY4_A="),
            Post("https://media.istockphoto.com/photos/search-engine-optimization-concept-picture-id1199839811?k=20&m=1199839811&s=612x612&w=0&h=T9YyXB-q3jFkTfWMJ34xMIjR4iJvgOJ16NdKP_YJPSw=", "Gaston", Timestamp.now(), "https://media.istockphoto.com/photos/search-engine-optimization-concept-picture-id1199839811?k=20&m=1199839811&s=612x612&w=0&h=T9YyXB-q3jFkTfWMJ34xMIjR4iJvgOJ16NdKP_YJPSw="),
            Post("https://media.istockphoto.com/photos/new-startup-project-millennials-business-team-hands-at-work-with-a-picture-id974512696?k=20&m=974512696&s=612x612&w=0&h=FU07i7S4KGcRS1Pf4XA_jtHHfagwdNCOAXgTpKL7sOU=", "Gaston", Timestamp.now(), "https://media.istockphoto.com/photos/new-startup-project-millennials-business-team-hands-at-work-with-a-picture-id974512696?k=20&m=974512696&s=612x612&w=0&h=FU07i7S4KGcRS1Pf4XA_jtHHfagwdNCOAXgTpKL7sOU=")
        )*/

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.latestsPosts.collect { result ->
                    when(result){

                        is Result.Loading -> {
                            binding.progressBar.show()
                        }
                        is Result.Success -> {
                            binding.progressBar.hide()
                            System.out.println(result.data)
                            if (result.data.isEmpty()){
                                binding.emptyContainer.show()
                                return@collect
                            }else{
                                binding.emptyContainer.hide()
                            }
                            binding.rvHome.adapter = HomeScreenAdapter(result.data, this)
                        }

                        is Result.Failure -> {
                            Toast.makeText(
                                requireContext(),
                                "ocurrio un error: ${result.exception}",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                }
            }
        }

       /* viewModel.fetchLatestPosts().observe(viewLifecycleOwner, Observer { result ->
            when(result){

                is Result.Loading -> {
                    binding.progressBar.show()
                }
                is Result.Success -> {
                    binding.progressBar.hide()
                    System.out.println(result.data)
                    if (result.data.isEmpty()){
                        binding.emptyContainer.show()
                        return@Observer
                    }else{
                        binding.emptyContainer.hide()
                    }
                    binding.rvHome.adapter = HomeScreenAdapter(result.data, this)
                }

                is Result.Failure -> {
                    Toast.makeText(
                        requireContext(),
                        "ocurrio un error: ${result.exception}",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        })*/

        //binding.rvHome.adapter = HomeScreenAdapter(postList)
    }

    override fun oLikeButtonClick(post: Post, liked: Boolean) {
        viewModel.registerLikeButtonState(post.id, liked).observe(viewLifecycleOwner) {
            viewModel.fetchLatestPosts().observe(viewLifecycleOwner, Observer { result ->
                when(result){

                    is Result.Loading -> { }
                    is Result.Success -> { }
                    is Result.Failure -> {
                        binding.progressBar.hide()
                        Toast.makeText(
                            requireContext(),
                            "ocurrio un error: ${result.exception}",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            })
        }
    }
}