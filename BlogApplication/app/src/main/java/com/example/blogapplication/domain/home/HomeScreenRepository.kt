package com.example.blogapplication.domain.home

import com.example.blogapplication.core.Result
import com.example.blogapplication.data.model.Post

interface HomeScreenRepository {

    suspend fun getLatestPost(): Result<List<Post>>
    suspend fun registerLikeButtonState(postId: String, liked: Boolean)
}