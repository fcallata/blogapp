package com.example.blogapplication.domain.camera

import android.graphics.Bitmap
import com.example.blogapplication.data.camera.CameraDataSource

class CamerRepoImpl(private val dataSource: CameraDataSource): CameraRepo {

    override suspend fun upload(imageBitmap: Bitmap, description: String) {
        dataSource.uploadPhoto(imageBitmap, description)
    }
}