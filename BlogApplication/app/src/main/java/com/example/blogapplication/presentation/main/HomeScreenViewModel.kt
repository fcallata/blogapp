package com.example.blogapplication.presentation.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.example.blogapplication.core.Result
import com.example.blogapplication.domain.home.HomeScreenRepository
import kotlinx.coroutines.Dispatchers
import java.lang.Exception

class HomeScreenViewModel(private val repo: HomeScreenRepository): ViewModel() {

    fun fetchLatestPosts() = liveData(Dispatchers.IO){
        emit(Result.Loading())

        kotlin.runCatching {
            repo.getLatestPost()
        }.onSuccess {postList ->
            emit(postList)
        }.onFailure {throwable ->
            emit(Result.Failure(Exception((throwable.message))))

        }
        /*try {
            emit(repo.getLatestPost())
        }catch (e: Exception){
            emit(Result.Failure(e))
        }*/
    }

    fun registerLikeButtonState(postId: String, liked: Boolean) = liveData(viewModelScope.coroutineContext + Dispatchers.Main){
        emit(Result.Loading())
        kotlin.runCatching {
            repo.registerLikeButtonState(postId, liked)
        }.onSuccess {
            emit(Result.Success(Unit))
        }.onFailure { throwable ->
            emit(Result.Failure(Exception(throwable.message)))
        }
    }
}

class HomeScreenViewModelFactory(private val repo: HomeScreenRepository): ViewModelProvider.Factory{
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(HomeScreenRepository::class.java).newInstance(repo)
    }

}