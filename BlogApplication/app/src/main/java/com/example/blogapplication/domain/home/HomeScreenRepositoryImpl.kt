package com.example.blogapplication.domain.home

import com.example.blogapplication.core.Result
import com.example.blogapplication.data.model.Post
import com.example.blogapplication.data.remote.HomeScreenDataSource

class HomeScreenRepositoryImpl(private val datasource: HomeScreenDataSource): HomeScreenRepository {

    override suspend fun getLatestPost(): Result<List<Post>> = datasource.getLatestPosts()
    override suspend fun registerLikeButtonState(postId: String, liked: Boolean) = datasource.registerLikeButtonState(postId, liked)
}