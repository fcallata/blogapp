package com.example.blogapplication.domain.auth

import android.graphics.Bitmap
import com.example.blogapplication.data.remote.AuthDatasource
import com.google.firebase.auth.FirebaseUser

class AuthRepositoryImpl(private val dataSource: AuthDatasource): AuthRepository {

    override suspend fun sigIn(email: String, password: String): FirebaseUser? = dataSource.signIn(email, password)
    override suspend fun signUp(email: String, password: String, username: String): FirebaseUser? = dataSource.signUp(email, password, username)
    override suspend fun updateProfile(imageBitmap: Bitmap, username: String) = dataSource.updateUserProfile(imageBitmap, username)
}