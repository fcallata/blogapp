package com.example.firstapplication.repository

import com.example.firstapplication.data.local.LocalMovieDataSource
import com.example.firstapplication.data.model.Movielist
import com.example.firstapplication.data.model.toMovieEntity
import com.example.firstapplication.data.remote.RemoteMovieDataSource

class MovieRepositoryImpl(
    private val dataSourceRemote: RemoteMovieDataSource,
    private val dataSourceLocal: LocalMovieDataSource
): MovieRepository {

    override suspend fun getUpcomingMovies(): Movielist{
        dataSourceRemote.getUpcomingMovies().results.forEach { movie ->
            dataSourceLocal.saveMovie(movie.toMovieEntity("upcoming"))
        }
        return dataSourceLocal.getUpcomingMovies()
    }

    override suspend fun gettopRatedMovies(): Movielist {
        dataSourceRemote.getTopRatedMovies().results.forEach{ movie ->
            dataSourceLocal.saveMovie(movie.toMovieEntity("toprated"))
        }
        return dataSourceLocal.getTopRatedMovies()
    }

    override suspend fun getPopularMovies(): Movielist{
        dataSourceRemote.getPopularMovies().results.forEach { movie ->
            dataSourceLocal.saveMovie(movie.toMovieEntity("popular"))
        }
        return dataSourceLocal.getPopularMovies()
    }


}