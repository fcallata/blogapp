package com.example.firstapplication.ui.movie

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import com.example.firstapplication.R
import com.example.firstapplication.core.Resource
import com.example.firstapplication.data.local.AppDatabase
import com.example.firstapplication.data.local.LocalMovieDataSource
import com.example.firstapplication.data.model.Movie
import com.example.firstapplication.data.remote.RemoteMovieDataSource
import com.example.firstapplication.databinding.FragmentMovieBinding
import com.example.firstapplication.presentation.MovieViewModel
import com.example.firstapplication.repository.MovieRepositoryImpl
import com.example.firstapplication.repository.RetrofitClient
import com.example.firstapplication.ui.movie.adapter.MovieAdapter
import com.example.firstapplication.ui.movie.adapter.concat.PopularConcatAdapter
import com.example.firstapplication.ui.movie.adapter.concat.TopRatedConcatAdapter
import com.example.firstapplication.ui.movie.adapter.concat.UpcomingConcatAdapter

class MovieFragment : Fragment(R.layout.fragment_movie), MovieAdapter.OnMoviewClickListener {

    private lateinit var binding: FragmentMovieBinding
    private val viewModel by viewModels<MovieViewModel> { MovieViewModel.MovieViewModelFactory(MovieRepositoryImpl(
        RemoteMovieDataSource(RetrofitClient.webservice),
        LocalMovieDataSource(AppDatabase.getDatabase(requireContext()).movieDao())
    ))}

    private lateinit var concatAdapter: ConcatAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMovieBinding.bind(view)
        concatAdapter = ConcatAdapter()

        viewModel.fetchMainScreenMovies().observe(viewLifecycleOwner, Observer { result ->
            when(result){
                is Resource.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    binding.progressBar.visibility = View.GONE
                    //Log.d("LiveData", "Upcoming: ${result.data.first} \n \n TopRated: ${result.data.second} \n \n Popular: ${result.data.third}")
                    concatAdapter.apply {
                        addAdapter(0,UpcomingConcatAdapter(MovieAdapter(result.data.first.results, this@MovieFragment)))
                        addAdapter(1,TopRatedConcatAdapter(MovieAdapter(result.data.second.results, this@MovieFragment)))
                        addAdapter(2,PopularConcatAdapter(MovieAdapter(result.data.third.results, this@MovieFragment)))
                    }

                    binding.rvMovies.adapter = concatAdapter
                }
                is Resource.Failure -> {
                    Log.d("Error", "${result.exception}")
                }
            }
        })

    }

    override fun onMovieClivk(movie: Movie) {
        //Log.d("Movie", "movieclick: ${movie}")
        val action = MovieFragmentDirections.actionMovieFragmentToMovieDetailFragment(
            movie.poster_path,
            movie.backdrop_path,
            movie.vote_average.toFloat(),
            movie.vote_count,
            movie.overview,
            movie.title,
            movie.original_language,
            movie.release_date
        )
        findNavController().navigate(action)
    }
}