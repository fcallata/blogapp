package com.example.firstapplication.repository

import com.example.firstapplication.application.AppConstans
import com.example.firstapplication.data.model.Movielist
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface WebService {

    @GET("movie/upcoming")
    suspend fun getUpcomingMovies(@Query("api_key") apiKey: String): Movielist

    @GET("movie/top_rated")
    suspend fun gettopRatedMovies(@Query("api_key") apiKey: String): Movielist

    @GET("movie/popular")
    suspend fun getPopularMovies(@Query("api_key") apiKey: String): Movielist
}

object RetrofitClient{

    //se inicializa en el momento que se llama
    val webservice by lazy {
        Retrofit.Builder()
            .baseUrl(AppConstans.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build().create(WebService::class.java)
    }
}