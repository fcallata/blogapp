package com.example.firstapplication.core

import java.lang.Exception

sealed class Resource<out T> {

    class Loading<out T>: Resource<T>()
    data class Success<out T>(val data: T): Resource<T>()
    class Failure(val exception: Exception): Resource<Nothing>()
}