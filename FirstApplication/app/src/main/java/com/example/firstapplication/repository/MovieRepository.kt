package com.example.firstapplication.repository

import com.example.firstapplication.data.model.Movielist

interface MovieRepository {

    suspend fun getUpcomingMovies(): Movielist

    suspend fun gettopRatedMovies(): Movielist

    suspend fun getPopularMovies(): Movielist
}