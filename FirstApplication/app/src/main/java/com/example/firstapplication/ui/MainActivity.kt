package com.example.firstapplication.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.firstapplication.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}