package com.example.firstapplication.data.remote

import com.example.firstapplication.application.AppConstans
import com.example.firstapplication.data.model.Movielist
import com.example.firstapplication.repository.WebService

class RemoteMovieDataSource (private val webService: WebService) {

    suspend fun getUpcomingMovies(): Movielist{
        return webService.getUpcomingMovies(AppConstans.API_KEY)
    }

    suspend fun getTopRatedMovies(): Movielist{
        return webService.gettopRatedMovies(AppConstans.API_KEY)
    }

    suspend fun getPopularMovies(): Movielist{
        return webService.getPopularMovies(AppConstans.API_KEY)
    }
}