package com.example.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.commit
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController

class PrimerFragment : Fragment(R.layout.fragment_primer) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val btn = view.findViewById<TextView>(R.id.btn_navegar)
        val resultado = view.findViewById<TextView>(R.id.text_first)

        setFragmentResultListener("requestKey"){ key, bundle ->
            val result = bundle.getString("bundleKey")
            resultado.text = result
        }

        btn.setOnClickListener {
            //lo comentamos para usar navigation component
            /*requireActivity().supportFragmentManager.commit {
                replace(R.id.fragment_container_view, SegundoFragment.newInstance("Fabio", 36))
                addToBackStack("primerFragnent")
            }*/
            //se comenta porque se usa el plugin de gradle
            //findNavController().navigate(R.id.action_primerFragment_to_segundoFragment, bundleOf("nombre" to "Ismael", "edad" to 33))
            val action = PrimerFragmentDirections.actionPrimerFragmentToSegundoFragment("callata", 18)
            findNavController().navigate(action)
        }
    }
}