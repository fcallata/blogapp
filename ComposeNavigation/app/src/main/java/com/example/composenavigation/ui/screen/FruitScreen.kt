package com.example.composenavigation.ui.screen

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Divider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.composenavigation.Screen
import com.example.composenavigation.data.Fruit
import com.example.composenavigation.data.fruitList

@Composable
fun FruitScreen( navController: NavController, fruitList: List<Fruit>) {
    LazyColumn {
        items(fruitList) { fruit ->
            FruitItem( fruit = fruit, onFruitClick = {
                navController.navigate(Screen.FruitDetailScreen.route + "/${fruit.id}")
            })
        }
    }
}

@Composable
fun FruitItem(fruit: Fruit, onFruitClick: (fruit: Fruit) -> Unit) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.Center
    ) {
        Text( text = fruit.name, modifier = Modifier
            .padding(start = 4.dp)
            .clickable {
                onFruitClick(fruit)
            })
    }

    Divider( )
}

@Preview(showBackground = true)
@Composable
fun FruitPreview(modifier: Modifier = Modifier) {
    Column {
        FruitItem(fruit = fruitList[0], onFruitClick = {})
    }

}

@Preview(showBackground = true)
@Composable
fun FruiScreenpreview() {
    FruitScreen( navController = rememberNavController(), fruitList)
}