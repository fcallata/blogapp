package com.example.composenavigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.composenavigation.data.findFruit
import com.example.composenavigation.data.fruitList
import com.example.composenavigation.ui.screen.FruitDetailScreen
import com.example.composenavigation.ui.screen.FruitScreen
import com.example.composenavigation.ui.screen.MasterScreen

sealed class Screen(val route: String) {
    object MasterScreen: Screen(route =  "master")
    object FruitScreen: Screen(route =  "fruit_screen")
    object FruitDetailScreen: Screen( route = "fruit_detail_screen")
}

@Composable
fun Navigation (modifier: Modifier = Modifier) {
    val navControler = rememberNavController()
    NavHost(navController = navControler, startDestination = Screen.MasterScreen.route ) {
        composable(Screen.MasterScreen.route) {
            MasterScreen(navControler)
        }

        composable(Screen.FruitScreen.route) {
            println(fruitList)
            FruitScreen(navControler, fruitList = fruitList)
        }

        composable( route = Screen.FruitDetailScreen.route + "/{id}", arguments = listOf(
            navArgument("id") {
                type = NavType.StringType
            }
        )){ navBackStackEntry ->
            navBackStackEntry.arguments?.getString("id")?.toInt()?.let { id ->
                println("nav id: " + id)
                val fruit = findFruit(id)
                if (fruit != null) {
                    FruitDetailScreen(navControler, fruit)
                }
            }
        }
    }
}
