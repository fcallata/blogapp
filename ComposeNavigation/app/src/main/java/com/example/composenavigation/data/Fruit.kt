package com.example.composenavigation.data

data class Fruit( val id: Int , val name: String)

val fruitList = listOf(Fruit(0, "Orange"),
    Fruit(1, "banana"),
    Fruit(2, "uva"),
    Fruit(3, "pera"))

fun findFruit(id: Int) = fruitList.find { it.id == id }