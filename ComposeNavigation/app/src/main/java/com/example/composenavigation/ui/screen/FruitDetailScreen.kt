package com.example.composenavigation.ui.screen

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.composenavigation.data.Fruit
import com.example.composenavigation.data.fruitList

@Composable
fun FruitDetailScreen(navController: NavController, fruit: Fruit) {
    if (fruit != null) {
        println("name: " + fruit.name)
    }
    if (fruit != null){
        Text(text = fruit.name)
    }else{
        println("else")
        //navController.popBackStack()
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewFruitDetailScreen() {
    FruitDetailScreen(navController = rememberNavController(), fruitList[1])
}