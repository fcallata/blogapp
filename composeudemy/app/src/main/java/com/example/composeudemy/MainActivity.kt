package com.example.composeudemy

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CardElevation
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

data class Recipe(
    @DrawableRes val imageResource: Int,
    val title: String,
    val ingredients: List<String>
)

val recipeList = listOf(Recipe(R.mipmap.header, "hola1", listOf("ingred1", "ingrediente2", "ingred3")),
    Recipe(R.mipmap.header, "hola2", listOf("ingred2", "ingrediente2", "ingred3")),
    Recipe(R.mipmap.header, "hola3", listOf("ingred3", "ingrediente2", "ingred3")),
    Recipe(R.mipmap.header, "hola4", listOf("ingred4", "ingrediente2", "ingred3")))

@Composable
private fun RecioreCar(recipe: Recipe, onRecipeClick: (Recipe) -> Unit){
    val image = painterResource(id = R.mipmap.header)
    Card ( shape = RoundedCornerShape(8.dp),
        elevation = CardDefaults.cardElevation(8.dp),
        modifier = Modifier
            .padding(8.dp)
            .clickable { onRecipeClick(recipe) }) {

        Column ( modifier = Modifier.padding(16.dp)){
            val imageModifier = Modifier
                .requiredHeight(150.dp)
                .fillMaxWidth()
            Image(painter = image, modifier = imageModifier, contentScale = ContentScale.Crop, contentDescription = "Foro recte")
            Spacer( modifier = Modifier.padding(  top = 10.dp))
            Text( text = recipe.title, style = MaterialTheme.typography.titleMedium)
            for (ingredient in recipe.ingredients) {
                Text( text = "* $ingredient", style = MaterialTheme.typography.bodyMedium)
            }
        }
    }
}

@Composable
private fun RecipeColumnList( recipelist: List<Recipe>) {
    LazyColumn {
        items(recipelist) { recipe ->
            RecioreCar( recipe = recipe, onRecipeClick = {
                Log.d("Recipe", "RecipeColumnList: ${it.title}")
            })
        }
    }
}


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            RecipeColumnList(recipeList)
        }
    }
}

@Composable
private fun NewsStory(){

    val image = painterResource(id = R.mipmap.header)
    val imageModifier = Modifier
        .requiredHeight(150.dp)
        .fillMaxWidth()
        .clip(shape = RoundedCornerShape(10.dp))

    Column (modifier = Modifier.padding(16.dp)) {

        Image( painter = image, contentDescription = "Header",
            modifier = imageModifier,
            contentScale = ContentScale.Crop)

        Spacer( modifier = Modifier.padding(top = 10.dp))

        Text( text = "Titlo de la noticia", style = MaterialTheme.typography.headlineMedium)
        Text( text = "Descripcion de la noticia", style = MaterialTheme.typography.bodyMedium)
        Text( text = "Pie de pagina de la noticia", style = MaterialTheme.typography.bodyMedium)
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewNewsStory() {
    RecioreCar(recipeList[0], onRecipeClick = {})
}