package com.example.cursoandroid

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class SegundaActivity : AppCompatActivity() {

    private var nombre: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_segunda)

        val btn = findViewById<Button>(R.id.btn_volver)
        val txt = findViewById<TextView>(R.id.txt_second)

        intent.extras?.let { bundle ->
            nombre = bundle.getString("nombre")
        }

        txt.text = nombre

        btn.setOnClickListener {
            val intent = Intent()
            intent.putExtra("nombre2", "Hola desde el sundo activity")
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }
}